//
//  ImageLoader.m
//  TestApp
//
//  Created by Fantom on 6/30/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "ImageLoader.h"
#import "ImageData.h"


@implementation ImageLoader {
    NSMutableData *activeDownload;
    NSURLConnection *imageConnection;
}

- (void)startDownload {
    activeDownload = [NSMutableData data];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:_imageData.imageURLString]];
    
    // alloc+init and start an NSURLConnection; release on completion/failure
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    imageConnection = connection;
}

- (void)cancelDownload {
    [imageConnection cancel];
    imageConnection = nil;
    activeDownload = nil;
}


#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [activeDownload appendData:data];  
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // Clear the activeDownload property to allow later attempts
    activeDownload = nil;
    
    // Release the connection now that it's finished
    imageConnection = nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    UIImage *downloadedImage = [[UIImage alloc] initWithData:activeDownload];
    UIImage *placeHolder     = [UIImage imageNamed:@"preloadedimg.jpg"];
    
    
    if (downloadedImage == nil) {
        _imageData.image = placeHolder;
    } else {
        _imageData.image = downloadedImage;

    }
    
    activeDownload = nil;
    
    // Release the connection now that it's finished
    imageConnection = nil;
    
    // call our delegate and tell it that our image is ready for display
    if (_completionHandler)
    {
        _completionHandler();
    }
}

@end