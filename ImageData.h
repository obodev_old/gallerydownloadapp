//
//  ImageData.h
//  TestApp
//
//  Created by Fantom on 6/30/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageData : NSObject

@property (nonatomic) UIImage  *image;
@property (nonatomic) NSString *imageURLString;
@property (nonatomic) BOOL     isSelected;

@end
