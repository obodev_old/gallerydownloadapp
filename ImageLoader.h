//
//  ImageLoader.h
//  TestApp
//
//  Created by Fantom on 6/30/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ImageData;

@interface ImageLoader : NSObject

@property (nonatomic, strong) ImageData *imageData;
@property (nonatomic, copy) void (^completionHandler)(void);

- (void)startDownload;
- (void)cancelDownload;

@end