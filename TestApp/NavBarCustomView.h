//
//  NavBarCustomView.h
//  TestApp
//
//  Created by Fantom on 6/6/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavBarCustomView : UIView

@property (nonatomic) UILabel *headerLabel;
@property (nonatomic) UILabel *photoCunterLabel;

@end
