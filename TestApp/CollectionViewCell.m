//
//  CollectionViewCell.m
//  TestApp
//
//  Created by Fantom on 6/6/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell {
    UIImageView *selectedView;
}

- (CollectionViewCell *)initWithFrame:(CGRect)rect {
    self = [super initWithFrame:rect];
    if (self) {
        _imageView             = [UIImageView new];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_imageView];
        
        selectedView        = [UIImageView new];
        selectedView.image  = [UIImage imageNamed:@"check2"];
        selectedView.hidden = YES;
        [self addSubview:selectedView];
        
        self.userInteractionEnabled = YES;
    }
    
    return  self;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (selected) {
        selectedView.hidden = NO;
    } else {
        selectedView.hidden = YES;
    }
}

- (void)layoutSubviews {
    _imageView.frame  = self.contentView.bounds;
    selectedView.frame = self.contentView.bounds;
}

@end
