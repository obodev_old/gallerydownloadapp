//
//  CollectionViewForCell.m
//  TestApp
//
//  Created by Fantom on 6/6/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "CollectionViewForTableVIewCell.h"
#import "CollectionViewCell.h"

@implementation CollectionViewForTableVIewCell

- (CollectionViewForTableVIewCell *)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout {
    self = [super initWithFrame:frame collectionViewLayout:layout];
    if (self) {
        self.allowsMultipleSelection = YES;
    }
    
    return self;
}

@end
