//
//  NavBarCustomView.m
//  TestApp
//
//  Created by Fantom on 6/6/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "NavBarCustomView.h"

@implementation NavBarCustomView

- (NavBarCustomView *)init {
    self = [super init];
    if (self) {
        _headerLabel               = [UILabel new];
        _headerLabel.font          = [UIFont fontWithName:@"ArialRoundedMTBold" size:16];
        _headerLabel.textAlignment = NSTextAlignmentCenter;
        _headerLabel.textColor     = [UIColor whiteColor];
        [self addSubview:_headerLabel];
        
        _photoCunterLabel               = [UILabel new];
        _photoCunterLabel.font          = [UIFont fontWithName:@"ArialRoundedMTBold" size:14];
        _photoCunterLabel.textAlignment = NSTextAlignmentCenter;
        _photoCunterLabel.textColor     = [UIColor lightGrayColor];
        [self addSubview:_photoCunterLabel];
    }
    
    return self;
}

- (void)layoutSubviews {
    static const int labelWidth   = 200;
    static const int labelHeight  = 20;
    static const int navBarHeight = 40;
    
    self.frame = CGRectMake(0,
                            0,
                            [[UIScreen mainScreen] bounds].size.width,
                            navBarHeight);
    _headerLabel.frame = CGRectMake(self.center.x - labelWidth/2,
                                    5,
                                    labelWidth,
                                    labelHeight);
    _photoCunterLabel.frame = CGRectMake(self.center.x - labelWidth/2,
                                        CGRectGetMaxY(_headerLabel.frame),
                                        labelWidth,
                                        labelHeight);
}

@end
