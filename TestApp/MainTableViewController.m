	//
//  MainTableViewController.m
//  TestApp
//
//  Created by Fantom on 6/6/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "MainTableViewController.h"
#import "PhotoTableViewCell.h"
#import "CollectionViewCell.h"
#import "NavBarCustomView.h"
#import "SectionHeaderView.h"

#import "ImageData.h"
#import "ImageLoader.h"

@implementation MainTableViewController {
    NavBarCustomView *navBarCustomView;
    
    //Array with Image loaders for each collection view cell
    NSMutableArray *imageDownloadsInProgress;
    
    //Array of BOOLs, indicating whether section is opened or not opened;
    NSMutableArray *openedSections;
    //Array of arrays with images for each collection view in each table view section
    NSMutableArray *allImagesArray;
    //All selected photos array
    NSMutableArray *selectedPhotosArray;
    //All randomly generated logos for header view in section
    NSMutableArray *setionsLogoArray;
    
    NSInteger photosSize;
}

static const int photosVerticalSpace     = 20;
static const int photosInRow             = 4;
static const int availablePhotosInAssets = 11;
static const int maxPhotosInSection      = 50;

static const int photosSections      = 50;
static const int sectionHeaderHeight = 44;
static NSString *cellIdentifier      = @"cellID";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self calculatePhotoSize];
    
    self.tableView.bounces         = YES;
    self.tableView.separatorStyle  = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor colorWithWhite:0.941 alpha:1.000];
    
    imageDownloadsInProgress = [NSMutableArray new];
    
    selectedPhotosArray = [NSMutableArray new];
    allImagesArray      = [NSMutableArray new];
    setionsLogoArray    = [NSMutableArray new];
    openedSections      = [NSMutableArray new];
    
    for (int i = 0; i < photosSections; i++) {
        [imageDownloadsInProgress addObject:[NSMutableDictionary dictionary]];
        [allImagesArray addObject:[NSMutableArray new]];
        [openedSections addObject:[NSNumber numberWithBool:NO]];
        [setionsLogoArray addObject:[NSString stringWithFormat:@"camera%d", arc4random_uniform(availablePhotosInAssets - 1) + 1]];
    }

    navBarCustomView                  = [NavBarCustomView new];
    navBarCustomView.headerLabel.text = @"Выбрать фото";
    
    [self updatePhotoCounter];
    [self.navigationController.navigationBar addSubview:navBarCustomView];
    [self.tableView registerClass:[PhotoTableViewCell class] forCellReuseIdentifier:cellIdentifier];
    [self showFirstSection];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return photosSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    BOOL sectionIsOpened =  [(NSNumber *)openedSections[section] boolValue];
    
    if (sectionIsOpened) {
        return 1;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    BOOL sectionIsOpened = [(NSNumber *)openedSections[section] boolValue];

    SectionHeaderView *sectionHeaderView = [SectionHeaderView new];
    sectionHeaderView.sectionLogo.image  = [UIImage imageNamed:setionsLogoArray[section]];
    sectionHeaderView.sectionTitle.text  = [NSString stringWithFormat:@"Album %d", section + 1];
    sectionHeaderView.tag                = section;
    [sectionHeaderView.tapGest addTarget:self action:@selector(sectionIsClicked:)];
    
    if (sectionIsOpened) {
        sectionHeaderView.sectionArrow.image = [UIImage imageNamed:@"arrowUp"];
        sectionHeaderView.buttonState        = (ButtonState *)directionUp;
    } else {
        sectionHeaderView.sectionArrow.image = [UIImage imageNamed:@"arrowDown"];
        sectionHeaderView.buttonState        = (ButtonState *)directionDown;
    }
    
    return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return sectionHeaderHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    static const int navBarHeight = 64;
//
//      Here we will calculate height for our table view cell. We have few requirements:
//  - if total images height in section is greater than table view cell height, we should display current cell header view, and next cell
//  header view
    int cellHeight                = [[UIScreen mainScreen] bounds].size.height - navBarHeight;
    
    int totalSections             = (int)[openedSections count];
    int currentSection            = (int)indexPath.section;
    int maxVisibleSectionHeaders  = 2;
    int imageRowsInCurrentSection = [self getImageRowsInSection:currentSection];
    int heightForCurrentSection;
    
    BOOL lastSectionIsOpened      = [(NSNumber *)openedSections[totalSections-1] boolValue];
    
//  if this is last section in table view, we won't have next section's header view
    if (lastSectionIsOpened) {
        maxVisibleSectionHeaders -= 1;
    }
//   We need to subtract visible sections headers
    cellHeight -= (sectionHeaderHeight * maxVisibleSectionHeaders);
    heightForCurrentSection = imageRowsInCurrentSection * (photosSize + photosVerticalSpace);
    
//   if total images height in section is lower than table view cell height, it will be the height for our cell
    if (heightForCurrentSection < cellHeight) {
        return heightForCurrentSection;
    }
    
    return cellHeight;
}

- (PhotoTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PhotoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell.collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:CollectionViewCellIdentifier];
    
    return cell;
}

#pragma mark Table View Delegate methods

-(void)tableView:(UITableView *)tableView willDisplayCell:(PhotoTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setCollectionViewDataSourceDelegate:self indexPath:indexPath];
}

#pragma mark Collection view methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(CollectionViewForTableVIewCell *)collectionView numberOfItemsInSection:(NSInteger)section {
    int targetPhotoArrayIndex = (int)collectionView.indexPathOfTableViewCell.section;
    
    return [allImagesArray[targetPhotoArrayIndex] count];
}

- (CollectionViewCell *)collectionView:(CollectionViewForTableVIewCell *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionViewCell *cell = (CollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier
                                                                                               forIndexPath:indexPath];
    
    ImageData *targetImgData = [self getTargetImageDataInSection:collectionView.indexPathOfTableViewCell.section
                                                             row:indexPath.row];
    
    if (!targetImgData.image) {
        // We will load only visible images on screen, while user is not dragging collection view
        if (collectionView.dragging == NO && collectionView.decelerating == NO) {
            [self startImageDownload:targetImgData inTableViewSection:collectionView.indexPathOfTableViewCell forIndexPath:indexPath];
        }
        // if a download is deferred or in progress, return a placeholder image
        cell.imageView.image = [UIImage imageNamed:@"Placeholder"];
    } else {
        cell.imageView.image = targetImgData.image;
    }
    

    if (targetImgData.isSelected) {
        [cell setSelected:targetImgData.isSelected];
        [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    }
    
    return cell;
}

#pragma mark Collection View Delegate methods

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPat {
    
    // User can select max 20 photos in all sections
    if (selectedPhotosArray.count < 20) {
        return YES;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка!"
                                                    message:@"Вы не можете выбрать больше 20 фото"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil,
                          nil];
  
    [alert show];

    return NO;
}

- (void)collectionView:(CollectionViewForTableVIewCell *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//  We need to cache collection view cell state (selected/not selected), as we have different collection views in different sections
    ImageData *targetImgData = [self getTargetImageDataInSection:collectionView.indexPathOfTableViewCell.section
                                                             row:indexPath.row];
    targetImgData.isSelected  = YES;
//    Adding selected image to array
    [selectedPhotosArray addObject:targetImgData.image];
//  Updating image counter
    [self updatePhotoCounter];
}

- (void)collectionView:(CollectionViewForTableVIewCell *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageData *targetImgData = [self getTargetImageDataInSection:collectionView.indexPathOfTableViewCell.section
                                                             row:indexPath.row];
    targetImgData.isSelected    = NO;
    
    [selectedPhotosArray removeObject:targetImgData.image];
    [self updatePhotoCounter];
}

#pragma mark Load pictures

- (void)startImageDownload:(ImageData *)imgData
        inTableViewSection:(NSIndexPath *)tableViewCellIndexPath
              forIndexPath:(NSIndexPath *)collectionCellIndexPath {
    
    //Get dictionary with loaders for appropriate table view section
    __block NSMutableDictionary *loadersDict = imageDownloadsInProgress[tableViewCellIndexPath.section];
    
    //Get image loader in dict by indexPath key
    ImageLoader *imgLoader = loadersDict[collectionCellIndexPath];
    
    //If there is no loader, we'r going to create one
    if (imgLoader == nil) {
        imgLoader = [ImageLoader new];
        imgLoader.imageData = imgData;
        
        [imgLoader setCompletionHandler:^{
            
            PhotoTableViewCell *tableCell      = (PhotoTableViewCell *)[self.tableView cellForRowAtIndexPath:tableViewCellIndexPath];
            CollectionViewCell *collectionCell = (CollectionViewCell *)[tableCell.collectionView cellForItemAtIndexPath:collectionCellIndexPath];
            
            collectionCell.imageView.image = imgData.image;
            [loadersDict removeObjectForKey:collectionCellIndexPath];
        }];
        
        loadersDict[collectionCellIndexPath] = imgLoader;
        [imgLoader startDownload];
    }
    
}

- (void)loadImagesForOnScreenRows {
    //First we are getting all visible table view cell's rows on screen
        NSArray *visibleSectionsOnTableView = [self.tableView indexPathsForVisibleRows];
    
        //Then, for each of visible table view cell's row, we are getting all visible cells in collection view
        for (NSIndexPath *sectionIndexPath in visibleSectionsOnTableView) {
            PhotoTableViewCell *tableCell = (PhotoTableViewCell *)[self.tableView cellForRowAtIndexPath:sectionIndexPath];
            NSArray *visibleImgsOnCollectionView = [tableCell.collectionView indexPathsForVisibleItems];
            
            //Here, we are getting ImageData for each visible collection view cell in our array (photoSectionsArray),
            //and checking if ImageData has already an image. If hasn't, we begin loading this image.
            for (NSIndexPath *imgIndexPaths in visibleImgsOnCollectionView) {
                ImageData *targetImgData = [self getTargetImageDataInSection:sectionIndexPath.section
                                                                         row:imgIndexPaths.row];
                
                if (!targetImgData.image) {
                    [self startImageDownload:targetImgData inTableViewSection:sectionIndexPath forIndexPath:imgIndexPaths];
                }

            }
        }
}

#pragma mark Scroll View Delegate methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
        [self loadImagesForOnScreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnScreenRows];
}

#pragma mark Photo counter mothod

- (void)updatePhotoCounter {
    static const int totalPhotos = 20;
    navBarCustomView.photoCunterLabel.text = [NSString stringWithFormat:@"%d из %d", (int)[selectedPhotosArray count], totalPhotos];
}

#pragma mark Section's header view methods

// Here we simulate tap gest on first section's header
- (void)showFirstSection {
    UITapGestureRecognizer *tap = [UITapGestureRecognizer new];
    SectionHeaderView *sectionHeaderView = [SectionHeaderView new];
    sectionHeaderView.tag = 0;
    [sectionHeaderView addGestureRecognizer:tap];
    
    [self sectionIsClicked:tap];
}

- (void)sectionIsClicked:(UITapGestureRecognizer *)tapGest {
//    Here we will handle tap event on header view of each section in table view
    self.view.userInteractionEnabled = NO;
    SectionHeaderView *targetView    = (SectionHeaderView *)tapGest.view;
    BOOL targetViewIsOpened          = [(NSNumber *)openedSections[targetView.tag] boolValue];
    
    int totalPhotosInTargetSection  = (int)[allImagesArray[targetView.tag] count];
    int minPhotos                   = 3;
    int totalPhotosToLoad           = arc4random_uniform(maxPhotosInSection - minPhotos) + minPhotos;
    int targetSection               = (int)targetView.tag;
    int minPhotoSize                = 100;

    NSMutableArray *targetArray = allImagesArray[targetSection];
    
//    if we open section first time, we need to fill an appropriate array with ImageData object
    if (!targetViewIsOpened && !totalPhotosInTargetSection) {
        for (int i = 0; i < totalPhotosToLoad; i++) {
            int photoSize              = arc4random_uniform(200) + minPhotoSize;
            NSString *kittenPhotoSize  = [NSString stringWithFormat:@"%d/%d", photoSize, photoSize];
            NSString *imageURLString   = [NSString stringWithFormat:@"http://placekitten.com/g/%@", kittenPhotoSize];
            
            ImageData *imageData     = [ImageData new];
            imageData.imageURLString = imageURLString;
            
            [targetArray addObject:imageData];
        }
    }
    
//  Then we open or close section with animation, and changing arrow direction
    openedSections[targetView.tag] = [NSNumber numberWithBool:!targetViewIsOpened];
    [targetView changeArrowDirection:^ {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:targetSection]
                          withRowAnimation:UITableViewRowAnimationNone];
            
            self.view.userInteractionEnabled = YES;
        });
    }];
}

#pragma mark Other methods

- (ImageData *)getTargetImageDataInSection:(NSInteger)section row:(NSInteger)row {
    NSMutableArray *targetArray = allImagesArray[section];
    
    return targetArray[row];
}

- (int)getImageRowsInSection:(int)section {
    int totalImages = (int)[allImagesArray[section] count];
    
    if (totalImages > 0) {
        return ((totalImages - 1) / photosInRow) + 1;
    }
    return 1;
}

- (void)calculatePhotoSize {
    int mainScreenWidth           = [UIScreen mainScreen].bounds.size.width;
    static const int imagesInRow  = 4;
    static const int sideEdge     = 5;
    
    photosSize =  (mainScreenWidth - (sideEdge * imagesInRow * 2)) / imagesInRow;
}

@end
