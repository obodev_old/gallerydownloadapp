//
//  PhotoTableViewCell.h
//  TestApp
//
//  Created by Fantom on 6/6/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionViewForTableVIewCell.h"

@interface PhotoTableViewCell : UITableViewCell

@property (nonatomic) CollectionViewForTableVIewCell *collectionView;

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate
                                  indexPath:(NSIndexPath *)indexPath;

@end
