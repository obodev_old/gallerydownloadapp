//
//  SectionHeaderView.h
//  TestApp
//
//  Created by Fantom on 6/6/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ButtonState) {
    directionDown,
    directionUp,
};

@interface SectionHeaderView : UIView

@property (nonatomic) UIImageView *sectionLogo;
@property (nonatomic) UILabel *sectionTitle;
@property (nonatomic) UIImageView *sectionArrow;

@property (nonatomic) UITapGestureRecognizer *tapGest;
@property (nonatomic) ButtonState *buttonState;


- (void)changeArrowDirection:(void(^)(void))completionBlock;

@end
