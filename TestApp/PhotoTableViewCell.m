//
//  PhotoTableViewCell.m
//  TestApp
//
//  Created by Fantom on 6/6/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "PhotoTableViewCell.h"

@implementation PhotoTableViewCell

- (PhotoTableViewCell *)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        int photoSize = [self calculatePhotoSize];
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
       [flowLayout setItemSize:CGSizeMake(photoSize, photoSize)];
       [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];

        self.collectionView = [[CollectionViewForTableVIewCell alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:CollectionViewCellIdentifier];
        self.collectionView.backgroundColor = [UIColor whiteColor];
        self.collectionView.showsVerticalScrollIndicator = NO;
        [self.contentView addSubview:self.collectionView];
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    self.collectionView.frame = self.contentView.bounds;
}

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate
                                  indexPath:(NSIndexPath *)indexPath {
    self.collectionView.dataSource               = dataSourceDelegate;
    self.collectionView.delegate                 = dataSourceDelegate;
    self.collectionView.indexPathOfTableViewCell = indexPath;
    
    [self.collectionView reloadData];
}

- (int)calculatePhotoSize {
    int mainScreenWidth           = [UIScreen mainScreen].bounds.size.width;
    static const int imagesInRow  = 4;
    static const int sideEdge     = 5;
    
    return (mainScreenWidth - (sideEdge * imagesInRow * 2)) / imagesInRow;
}

@end
