//
//  SectionHeaderView.m
//  TestApp
//
//  Created by Fantom on 6/6/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "SectionHeaderView.h"

@implementation SectionHeaderView

- (SectionHeaderView *)init {
    self = [super init];
    if (self) {
        _sectionLogo = [UIImageView new];
        [self addSubview:_sectionLogo];
        
        _sectionTitle               = [UILabel new];
        _sectionTitle.font          = [UIFont fontWithName:@"ArialRoundedMTBold" size:16];
        _sectionTitle.textAlignment = NSTextAlignmentLeft;
        _sectionTitle.textColor     = [UIColor blackColor];
        [self addSubview:_sectionTitle];

        _sectionArrow       = [UIImageView new];
        [self addSubview:_sectionArrow];
        
        _tapGest = [UITapGestureRecognizer new];
        [self addGestureRecognizer:_tapGest];
        
        self.backgroundColor = [UIColor colorWithWhite:0.820 alpha:1.000];
        self.exclusiveTouch = YES;
    }
    
    return self;
}

- (void)layoutSubviews {
    const int sectionWidth             = [[UIScreen mainScreen] bounds].size.width;
    static const int sectionTitleWidth = 200;
    static const int sectionPhotoSize  = 30;
    static const int indent            = 7;
    
    _sectionLogo.frame = CGRectMake(indent,
                                    indent,
                                    sectionPhotoSize,
                                    sectionPhotoSize);
    _sectionTitle.frame = CGRectMake(CGRectGetMaxX(_sectionLogo.frame) + indent,
                                     _sectionLogo.frame.origin.y,
                                     sectionTitleWidth,
                                     sectionPhotoSize);
    _sectionArrow.frame = CGRectMake(sectionWidth - indent - sectionPhotoSize,
                                     _sectionLogo.frame.origin.y,
                                     sectionPhotoSize,
                                     sectionPhotoSize);
}

#pragma mark Rotations

- (void)changeArrowDirection:(void(^)(void))completionBlock {
    if (self.buttonState == directionDown) {
        [self rotateArrowClockWise:YES complitionBlock:completionBlock];
        self.buttonState = (ButtonState *)directionUp;
        return;
    }
    
    [self rotateArrowClockWise:NO complitionBlock:completionBlock];
    self.buttonState = (ButtonState *)directionDown;
}

- (void)rotateArrowClockWise:(BOOL)clockWise complitionBlock:(void(^)(void))completionBlock {
    NSArray *values = @[@0, @-3.2];
    int angle = -180;
    
    if (clockWise) {
        values = @[@0, @3.2];
        angle = 180;
    }
    
    [CATransaction begin];
    
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
    animation.keyPath              = @"transform.rotation";
    animation.values               = values;
    animation.duration             = 0.5;
    animation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    
    if (completionBlock != nil) {
        [CATransaction setCompletionBlock:completionBlock];
    }
    
    [_sectionArrow.layer addAnimation:animation forKey:@"rotation"];
    _sectionArrow.transform = CGAffineTransformMakeRotation(angle * M_PI/180);
    
    [CATransaction commit];
}

@end
