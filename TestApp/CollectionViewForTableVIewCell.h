//
//  CollectionViewForCell.h
//  TestApp
//
//  Created by Fantom on 6/6/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *CollectionViewCellIdentifier = @"CellCollectionViewIdentifier";

@interface CollectionViewForTableVIewCell : UICollectionView

@property (nonatomic) NSIndexPath *indexPathOfTableViewCell;

@end
