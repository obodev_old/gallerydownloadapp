This app was created in order to show code syntax. The app is pretty simple, there are N photo albums (we’ve set it to 50:-), and  each photo album contain  N  photos.(we’re setting is randomly from 3 to 50). There are four images in row, their size will be calculated depending on the phone’s screen.

All photos are loading from http://placekitten.com web site asynchronously. Only visible photos on screen will be loaded, while user isn’t scrolling album. Height for opened album will be calculated, based on images quantity with following requirements:
  - if images don’t fit our screen, we would display current album header, visible photos, and next album header at the bottom 
  - if images fit our screen, we will reduce album size until last photo

You can select photos in different albums. Green check mark will be shown, and photo’s counter will be updated. All selected photos will remain selected even if you close an album and reopen it. All selected photos will be added to an array. User can select max of 20 photos in all albums, after that he will see an error alert.